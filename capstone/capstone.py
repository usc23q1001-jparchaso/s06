from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def get_full_name(self):
        pass

    @abstractclassmethod
    def add_request(self):
        pass

    @abstractclassmethod
    def check_request(self):
        pass

    @abstractclassmethod
    def add_user(self):
        pass

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'

    def add_request(self):
        return f'Request has been added'

    def check_request(self):
        print(f'Requested')

    def add_user(self):
        return f'User has been added'

    def login(self):
        return f'{self._email} has logged in'

    def logout(self):
        return f'{self._email} has logged out'

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []

    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'

    def check_request(self):
        print(f'Requested')

    def add_request(self):
        return f'Request has been added'

    def add_user(self):
        return f'User has been added'

    def login(self):
        return f'{self._email} has logged in'

    def logout(self):
        return f'{self._email} has logged out'

    def add_member(self, first_name):
        self._members.append(first_name)
        self._first_name = first_name

    def get_members(self):
        return self._members

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'

    def add_request(self):
        return f'Request has been added'

    def check_request(self):
        print('Requested')

    def login(self):
        return f'{self._email} has logged in'

    def logout(self):
        return f'{self._email} has logged out'

    def add_user(self):
        return f'User has been added'

class Request:
    def __init__(self, name, requester, date_requested):
        super().__init__()
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self._status = ''

    def update_request(self):
        print('Request has been updated')

    def close_request(self):
        print('Request has been closed')

    def cancel_request(self):
        print('Request has been cancelled')

    def get_full_name(self):
        return f'{self._first_name} {self._last_name}'

    def add_request(self):
        return f'Request has been added'

    def check_request(self):
        print('Requested')

    def set_status(self, status):
        self._status = status

    def closed_request(self):
        return f'Request {self._name} has been closed'

    def add_user(self):
        return f'User has been added'

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
team_lead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", team_lead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_full_name() == 'John Doe', 'Full name should be John Doe'
assert admin1.get_full_name() == 'Monika Justin', 'Full name should be Monika Justin'
assert team_lead1.get_full_name() == 'Michael Specter', 'Full name should be Michael Specter'
assert employee2.login() == 'sjane@mail.com has logged in'
assert employee2.add_request() == 'Request has been added'
assert employee2.logout() == 'sjane@mail.com has logged out'

team_lead1.add_member(employee3)
team_lead1.add_member(employee4)
for indiv_emp in team_lead1.get_members():
    print(indiv_emp.get_full_name())
assert admin1.add_user() == 'User has been added'
req2.set_status('closed')
print(req2.closed_request())